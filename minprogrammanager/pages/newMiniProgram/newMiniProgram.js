// pages/newMiniProgram/newMiniProgram.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    merchantName:"",
    merchantCode:"",
    merchantCodeType:"",
    legalPersonName:"",
    legalPersonWechatNum:"",
    merchantPhone:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.itemStr != null) {
      var decodeJsonStr = decodeURIComponent(options.itemStr)
      var item = JSON.parse(decodeJsonStr)
      if (item != null) {
        this.setData({
          merchantName:item.merchantName,
          merchantCode:item.merchantCode,
          merchantCodeType:item.merchantCodeType,
          legalPersonName:item.legalPersonName,
          legalPersonWechatNum:item.legalPersonWechatNum,
          merchantPhone:item.merchantPhone,
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  requestCreateMiniProgram:function() {
    var that = this
    let app = getApp()
    var appid = "wxbc523f38a4caf22f"//微信第三方平台
    var appSecret = "42f5ef5dec2ada83c7971d004b9b3815"
    let url = app.globalData.ThirdParty_URL + "/createMiniProgram"+"?appid="+appid+"&appSecret="+appSecret
    wx.request({
      url: url,
      method:'POST',
      data:{"merchantName":this.data.merchantName,"merchantCode":this.data.merchantCode,"merchantCodeType":Number(this.data.merchantCodeType),"legalPersonWechatNum":this.data.legalPersonWechatNum,"legalPersonName":this.data.legalPersonName,"merchantPhone":this.data.merchantPhone},
      success:function(res){
        console.log("finihsi create")
        console.log(res.data)
        let success = res.data.success
        if(success==1) {//success
          wx.navigateBack({
            delta: 0,
          })
        }
      },
      fail:function(err){
          console.log("fail create mini")
          console.log(err)
          wx.showToast({
            title: err,
          })
      }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  tapListButton:function() {

  },
  tapOk:function() {
    if(this.data.merchantName == null || this.data.merchantName.length <= 0) {
      wx.showToast({
        title: '企业名未输入',
      })
      return
    }
    if(this.data.merchantCode == null || this.data.merchantCode.length <= 0) {
      wx.showToast({
        title: '企业代码未输入',
      })
      return
    }
    if(this.data.merchantCodeType == null || this.data.merchantCodeType.length <= 0) {
      wx.showToast({
        title: '企业代码类型未输入',
      })
      return
    }
    if(this.data.legalPersonName == null || this.data.legalPersonName.length <= 0) {
      wx.showToast({
        title: '法人姓名未输入',
      })
      return
    }
    if(this.data.legalPersonWechatNum == null || this.data.legalPersonWechatNum.length <= 0) {
      wx.showToast({
        title: '法人微信号未输入',
      })
      return
    }
    if(this.data.merchantPhone == null || this.data.merchantPhone.length <= 0) {
      wx.showToast({
        title: '商户联系方式未输入',
      })
      return
    }
    this.requestCreateMiniProgram()
  },
  inputMerchantName:function(e) {
    this.setData({
      merchantName:e.detail.value
    })
  },
  inputMerchantCode:function(e) {
    this.setData({
      merchantCode:e.detail.value
    })
  },
  inputMerchantCodeType:function(e) {
    this.setData({
      merchantCodeType:e.detail.value
    })
  },
  inputLegalName:function(e) {
    this.setData({
      legalPersonName:e.detail.value
    })
  },
  inputLegalWxNum:function(e) {
    this.setData({
      legalPersonWechatNum:e.detail.value
    })
  },
  inputPhone:function(e) {
    this.setData({
      merchantPhone:e.detail.value
    })
  }
})