// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    //this.requestHello()
    //this.requestTestXml()
  },
  requestHello:function() {
    var that = this
    let app = getApp()
    let url = app.globalData.ThirdParty_URL + "/hello"
    
    wx.request({
      url: url,
      method:'GET',
      success:function(res){
        console.log(res.data)
        let success = res.data.success
        if(success==1) {//success
        }
      },
      fail:function(err){
          console.log(err)
      }
    })
  },
  requestTestXml:function() {
    var that = this
    let url = app.globalData.ThirdParty_URL + "/testXml"
    let str = "<xml><AppId><![CDATA[wxbc523f38a4caf22f]]></AppId><CreateTime>1614575286</CreateTime><InfoType><![CDATA[component_verify_ticket]]></InfoType><ComponentVerifyTicket><![CDATA[ticket@@@esKoBazVA8lQ9guIwbl308kO5l7RkK_9vvb8NgX3jRnxSoxQZFWlYZHk5Q_TCBu_n3QyI8kQpd9jxZMrtJ1UKQ]]></ComponentVerifyTicket></xml>"

    let str1 = "<xml><AppId><![CDATA[wxbc523f38a4caf22f]]></AppId><CreateTime>1614934163</CreateTime><InfoType><![CDATA[notify_third_fasteregister]]></InfoType><status>100002</status><msg><![CDATA[face check failed]]></msg><info><name><![CDATA[上海迈息信息科技有限公司]]></name><code><![CDATA[91310114MA1GXK9N3R]]></code><code_type>1</code_type><legal_persona_wechat><![CDATA[x7656456345636786]]></legal_persona_wechat><legal_persona_name><![CDATA[肖成]]></legal_persona_name><component_phone><![CDATA[13915341079]]></component_phone></info></xml>"
    wx.request({
      url: url,
      method:'POST',
      data:str,
      success:function(res){
        console.log("finihsi create")
        console.log(res.data)
        let success = res.data.success
        if(success==1) {//success
          
        }
      },
      fail:function(err){
          console.log("fail create mini")
          console.log(err)
      }
    })
  },
  tapNewBtn:function() {
    wx.navigateTo({
      url: '../newMiniProgram/newMiniProgram',
    })
  },
  tapListButton:function() {
    wx.navigateTo({
      url: '../newCreateList/newCreateList',
    })
  }
})
