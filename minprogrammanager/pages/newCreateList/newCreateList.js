// pages/newCreateList/newCreateList.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    createReqList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.requestCreateList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  requestCreateList:function() {
    var that = this
    let app = getApp()    
    let url = app.globalData.ThirdParty_URL + "/getCreateMiniProgramList"
    wx.request({
      url: url,
      success:function(res){
        console.log(res.data)
        if(res.data.success == 1) {
          var list = res.data.list
          if(list != null) {
            that.setData({
              createReqList:list
            })
          }
        }
      },
      fail:function(err){
          console.log(err)
      }
    })
  },
  tapItem:function(e) {
    let idx = e.currentTarget.dataset.idx
    let item = this.data.createReqList[idx]
    var jsonStr = JSON.stringify(item)
    var encodeJsonStr = encodeURIComponent(jsonStr)
    wx.navigateTo({
      url: '../newMiniProgram/newMiniProgram?itemStr='+encodeJsonStr,
    })
  }
})